#### 语义分割训练前处理工具
详细说明在：[CSDN链接](https://blog.csdn.net/qq_39330520/article/details/108219002)
#### 运行
git clone或下载程序后，运行.exe文件可直接运行
或运行SegmentationTool_UI_Complete.py也可打开界面进行操作

#### 主要功能

| 名称            | 功能                                                         |
| --------------- | ------------------------------------------------------------ |
| 视频转图像      | 将视频素材转换成单张图像                                     |
| JSON/JPG分离    | 把标注后json文件和对应的jpg文件从混合在一起的一个文件夹中提取到相同(或不同)的文件夹中 |
| JSON_TO_Dataset | 从标注得到的Json文件获得标签文件                             |
| Get_JPG_PNG     | 从上一步的Dataset文件中提取训练图像和训练标签                |
| 随机提取验证集  | 从训练集中随机选取一定比例的图像和标签作为验证集图像和标签   |
| MIOU/MPA计算    | 由模型输出标签图像和人工标签图像计算得到MIOU和MPA            |

![images](https://images.gitee.com/uploads/images/2020/0824/104232_a3ed91aa_5558625.png)

#### 程序说明

1. draw.py   调用所需文件
2. SegmentationTool.ui   QT界面文件
3. SegmentationTool.py   PYUIC生成的界面py文件
4. SegmentationTool_UI.py   功能函数py文件
5. SegmentationTool_UI_Complete.py   完整版程序，界面和功能函数在一起，直接运行

#### BY LiangBo